package cuenta.banco;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.servlet.Servlet;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import persistencia.controladores.controladorCuenta;
import persistencia.controladores.controladorUsuario;
import persistencia.controladores.controladorTransaccion;
import persistencia.gui.GUI_Cuenta;

/**
 * Hecho por Santiago Arenas Palacio
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	//para el tipo de cuenta 1 es para ahorros y 2 para corriente
    	/*GUI_Cuenta gui = new GUI_Cuenta();
        gui.iniciar();*/
    	


    	Server server = new Server(8080);
        server.setHandler(new DefaultHandler());

        ServletContextHandler contextCuenta = new ServletContextHandler();
        ServletContextHandler contextUsuario = new ServletContextHandler();
        ServletContextHandler contextTransaccion = new ServletContextHandler();
        
    	
       /*contextCuenta.setContextPath("/");
       //context.addServlet((Class<? extends Servlet>) controlador.class,"/cuenta/*");
       contextCuenta.addServlet(controladorCuenta.class,"/cuenta/*");
       server.setHandler(contextCuenta);*/
       
       contextUsuario.setContextPath("/");
       //context.addServlet((Class<? extends Servlet>) controlador.class,"/cuenta/*");
       contextUsuario.addServlet(controladorUsuario.class,"/usuario/*");
        server.setHandler(contextUsuario);
       
       /*contextTransaccion.setContextPath("/");
       //context.addServlet((Class<? extends Servlet>) controlador.class,"/cuenta/*");
       contextTransaccion.addServlet(controladorTransaccion.class,"/transaccion/*");
       server.setHandler(contextTransaccion);*/
        
        
        try{
            server.start();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
