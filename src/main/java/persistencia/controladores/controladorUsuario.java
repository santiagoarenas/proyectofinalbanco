package persistencia.controladores;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import persistencia.servicios.ServiciosUsuario;

import persistencia.entidades.Usuario;

public class controladorUsuario extends HttpServlet{
	
	private ServiciosUsuario servicios;
	private  ObjectMapper mapper;
	public controladorUsuario() 
	{
		servicios = new ServiciosUsuario();
        mapper = new ObjectMapper();
	}
	
	//metodo para buscar y listar usuarios 
	@Override
	protected void doGet(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException,IOException
	{		
		String path = peticion.getPathInfo();
        if (path == null) {
            List<Usuario> usuario = servicios.listarUsuarios();
            String json = mapper.writeValueAsString(usuario);
            respuesta.setContentType("application/json");
            respuesta.getWriter().println(json);
        } else {
            switch (path) {
                case "/buscar":
                    String cedula = peticion.getParameter("cedula");
                    try {
                        Usuario usuario = servicios.buscarUsuario(cedula);
                        String json = mapper.writeValueAsString(usuario);
                        respuesta.setContentType("application/json");
                        respuesta.getWriter().println(json);
                    } catch (Exception e) {
                    	respuesta.setStatus(404);
                        Map<String, String> error = new HashMap<>();
                        error.put("error", e.getMessage());
                        String json = mapper.writeValueAsString(error);
                        respuesta.setContentType("application/json");
                        respuesta.getWriter().println(json);
                    }
                    break;
                default:
                	respuesta.setStatus(404);
                    Map<String, String> error = new HashMap<>();
                    error.put("error", "No se encontro el recurso");
                    String json = mapper.writeValueAsString(error);
                    respuesta.setContentType("application/json");
                    respuesta.getWriter().println(json);
                    break;
            }
        }
	}

	@Override
	//metodo para crear un usuario
	protected void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException,IOException

	{
		String content = peticion.getContentType();
		if(content != null && (content == "application/json")) 
		{
			Map<String, Object> usuariomap = mapper.readValue(peticion.getInputStream(), HashMap.class);
	        try {
	            	servicios.guardarUsuario(usuariomap);
	            	//respueta.setStatus(HttpServletResponse.SC_CREATED);
	            	respuesta.setStatus(201);
	            	Map<String, String> respuestamap = new HashMap<>();
	            	respuestamap.put("mensaje", "usuario guardado con exito");
	            	String json = mapper.writeValueAsString(respuestamap);
	            	respuesta.setContentType("application/json");
	            	respuesta.getWriter().println(json);

	        	} catch (Exception e) 
	        		{
	        			//respuesta.setStatus(HttpServletResponse.SC_CONFLICT);
	        			respuesta.setStatus(409);
	        			Map<String, String> error = new HashMap<>();
	        			error.put("error", e.getMessage());
	        			String json = mapper.writeValueAsString(error);
	        			respuesta.setContentType("application/json");
	        			respuesta.getWriter().println(json);
	        		}
		}else
			{
				//respuesta.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
				respuesta.setStatus(415);
				Map<String, String> error = new HashMap<>();
				error.put("error", "El contenido debe ser JSON");
				String json = mapper.writeValueAsString(error);
				respuesta.setContentType("application/json");
				respuesta.getWriter().println(json);
			}
	}
	
	protected void doDelete(HttpServletRequest peticion, HttpServletResponse respuesta) throws ServletException, IOException 
	{
		String cedula = peticion.getParameter("cedula");
        try {
            servicios.eliminarUsuario(cedula);

            //response.setStatus(HttpServletResponse.SC_OK);
            respuesta.setStatus(200);
            Map<String, String> respuestamap = new HashMap<>();
            respuestamap.put("mensaje", "usuario eliminado con exito");
            String json = mapper.writeValueAsString(respuestamap);
            respuesta.setContentType("application/json");
            respuesta.getWriter().println(json);
        } catch (Exception e) {
            //respuesta.setStatus(HttpServletResponse.SC_CONFLICT);
        	respuesta.setStatus(409);
            Map<String, String> error = new HashMap<>();
            error.put("error", e.getMessage());
            String json = mapper.writeValueAsString(error);
            respuesta.setContentType("application/json");
            respuesta.getWriter().println(json);
        }
	}
}
