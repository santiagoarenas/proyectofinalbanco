package persistencia.controladores;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import persistencia.servicios.serviciosTransaccion;

public class controladorTransaccion extends HttpServlet {

	private serviciosTransaccion servicios;
	private  ObjectMapper mapper;
	public controladorTransaccion() 
	{
		servicios = new serviciosTransaccion();
        mapper = new ObjectMapper();
	}
	
	protected void doPost(HttpServletRequest peticion,HttpServletResponse respuesta) throws ServletException,IOException

	{
		String content = peticion.getContentType();
		if(content != null && (content == "application/json")) 
		{
			Map<String, Object> transaccionmap = mapper.readValue(peticion.getInputStream(), HashMap.class);
	        try {

            	
	            	servicios.depositarDinero(transaccionmap);
	            	//respueta.setStatus(HttpServletResponse.SC_CREATED);
	            	respuesta.setStatus(201);
	            	Map<String, String> respuestamap = new HashMap<>();
	            	respuestamap.put("mensaje", "transaccion exitosa");
	            	String json = mapper.writeValueAsString(respuestamap);
	            	respuesta.setContentType("application/json");
	            	respuesta.getWriter().println(json);

	        	} catch (Exception e) 
	        		{
	        			//respuesta.setStatus(HttpServletResponse.SC_CONFLICT);
	        			respuesta.setStatus(409);
	        			Map<String, String> error = new HashMap<>();
	        			error.put("error ", e.getMessage());
	        			String json = mapper.writeValueAsString(error);
	        			respuesta.setContentType("application/json");
	        			respuesta.getWriter().println(json);
	        		}
		}else
			{
				//respuesta.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
				respuesta.setStatus(415);
				Map<String, String> error = new HashMap<>();
				error.put("error", "El contenido debe ser JSON");
				String json = mapper.writeValueAsString(error);
				respuesta.setContentType("application/json");
				respuesta.getWriter().println(json);
			}
	}
	
}
