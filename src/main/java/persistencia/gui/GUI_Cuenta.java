package persistencia.gui;

import java.util.List;
import java.util.Scanner;

import persistencia.entidades.Cuenta;
import persistencia.servicios.ServiciosCuentas;

public class GUI_Cuenta {

	private boolean running = true;
    private ServiciosCuentas serviciosCuenta;

    public GUI_Cuenta() {
    	serviciosCuenta = new ServiciosCuentas();
    }

    public void iniciar() {
        System.out.println("Bienvenido al sistema de persistencia de cuentas");

        while (running) {
            System.out.println("1. Crear cuenta");
            System.out.println("2. Listar cuentas");
            System.out.println("3. Buscar cuenta");
            System.out.println("4. Eliminar cuenta");
            System.out.println("5. retirar");
            System.out.println("6. depositar");
            System.out.println("7. transferir dinero");
            System.out.println("8. Salir");
            Scanner scanner = new Scanner(System.in);
            int opcion = scanner.nextInt();
            seleccion(opcion);
        }

    }

    private void seleccion(int seleccion) {
        switch (seleccion) {
            case 1:
                crearCuenta();
                break;
            case 2:
                listarCuentas();
                break;
            case 3:
                buscarCuenta();
                break;
            case 4:
                eliminarCuenta();
                break;
            case 5:
            	retirar();
            	break;
            case 6:
            	depositar();
            	break;
            case 7:
                transferir();
                break;
            case 8:
            	salir();
            default:
                System.out.println("Opcion no valida");
                break;
        }
    }

    private void crearCuenta() {
        System.out.println("Crear Cuenta");
        Scanner scanner = new Scanner(System.in);
        System.out.println("numero cuenta: ");
        String numeroCuenta = scanner.nextLine();
        System.out.println("tipoCuenta: ");
        String tipoCuenta = scanner.nextLine();
        System.out.println("idUsuario: ");
        String idUsuario = scanner.nextLine();
        

        Cuenta cuenta = new Cuenta(numeroCuenta, tipoCuenta, idUsuario);
        serviciosCuenta.guardarCuenta(cuenta);

    }

    private void listarCuentas() {
        System.out.println("Listando cuentas");
        //List<Cuenta> cuentasEnBaseDatos = serviciosCuenta.listarCuentas();

        System.out.println("numero cuenta |  saldo  |  tipo cuenta | propietario | ");
        
        /*for (Cuenta cuentaEnBaseDatos : cuentasEnBaseDatos) 
        {
        	System.out.println(cuentaEnBaseDatos.getNumeroCuenta() +"               "
  				  +cuentaEnBaseDatos.getSaldo()+"     "
  				  +cuentaEnBaseDatos.getTipoCuenta()+"         "
  				  +cuentaEnBaseDatos.getidUsuario()+"                ");
        }
        
        System.out.println("\n");
        /*for (Cuenta cuentaEnBaseDatos : cuentasEnBaseDatos) {
            System.out.println("numero cuenta: "+cuentaEnBaseDatos.getNumeroCuenta()+"\n"
            				  +"saldo: "+cuentaEnBaseDatos.getSaldo()+"\n"
            				  +"propietario: "+cuentaEnBaseDatos.getPropietario()+"\n"
            				  +"tipo cuenta: "+cuentaEnBaseDatos.getTipoCuenta()+"\n"
            				  +"numero de depositos: "+cuentaEnBaseDatos.getNumeroDepositos()+"\n"
            				  +"numero de Retiros: "+cuentaEnBaseDatos.getNumeroRetiros()+"\n");
            				  
        }*/
    }

    private void buscarCuenta() {
        System.out.println("Buscar cuenta");
        Scanner scanner = new Scanner(System.in);
        System.out.println("numero cuenta: ");
        String numeroCuenta = scanner.nextLine();
        try {
            Cuenta encontrada = serviciosCuenta.buscarCuenta(numeroCuenta);
            System.out.println("--------CUENTA ENCONTRADA------\n"
            	  +"numero cuenta: "+encontrada.getNumeroCuenta()+"\n"
  				  +"saldo: "+encontrada.getSaldo()+"\n"
  				  +"tipo cuenta: "+encontrada.getTipoCuenta()+"\n"
  				  +"propietario: "+encontrada.getidUsuario()+"\n");
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    private void eliminarCuenta() {
        System.out.println("Eliminar cuenta");
        Scanner scanner = new Scanner(System.in);
        System.out.println("numero cuenta: ");
        String numeroCuenta = scanner.nextLine();
        try {
            serviciosCuenta.eliminarcuenta(numeroCuenta);
            System.out.println("Cuenta eliminada");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void retirar() 
    {
    	System.out.println("----RETIRO DE DINERO DE CUENTA");
    	Scanner entrada = new Scanner(System.in);
    	System.out.println("ingrese su numero de cuenta");
    	String numeroCuenta = entrada.nextLine();
    	System.out.println("cuanto dinero desea retirar");
    	float montoRetirarDinero = entrada.nextFloat();
    	try 
    	{
    		serviciosCuenta.retirarDinero(numeroCuenta, montoRetirarDinero);
    	}catch(Exception e) 
    		{
    			System.out.println(e.getMessage());
    		}
    }
    
    private void depositar() 
    {
    	System.out.println("----DEPOSITO DE DINERO DE CUENTA");
    	Scanner entrada = new Scanner(System.in);
    	System.out.println("ingrese su numero de cuenta");
    	String numeroCuenta = entrada.nextLine();
    	System.out.println("cuanto dinero desea depositar");
    	float montoRetirarDinero = entrada.nextFloat();
    	try 
    	{
    		serviciosCuenta.depositarDinero(numeroCuenta, montoRetirarDinero);
    	}catch(Exception e) 
    		{
    			System.out.println(e.getMessage());
    		}
    }
    
    private void transferir() 
    {
    	System.out.println("-----TRANSFERIR DINERO");
    	Scanner entrada = new Scanner(System.in);
    	System.out.println("ingrese el numero de cuenta desde donde va hacer la transferencia");
    	String numeroCuentaUno = entrada.nextLine();
    	System.out.println("ingrese el numero de cuenta a donde quiere hacer la transferencia");
    	String numeroCuentaDos = entrada.nextLine();
    	System.out.println("ingrese el dinero que desea transferir");
    	float montoTransferencia = entrada.nextFloat();
    	try 
    	{
			serviciosCuenta.transferirDinero(numeroCuentaUno, numeroCuentaDos, montoTransferencia);
    	}catch(Exception e) 
    		{
    			System.out.println(e.getMessage());
    		}
    }
    private void salir() {
        System.out.println("Salir");
        running = false;
    }
}
