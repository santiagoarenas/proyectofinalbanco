package persistencia.repositorio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import persistencia.entidades.Cuenta;
import persistencia.entidades.Usuario;

public class CuentaBaseDatos implements Repositorio{
	
	private String cadenaConexion;

    public CuentaBaseDatos() {
        try {
            DriverManager.registerDriver(new org.sqlite.JDBC());
            cadenaConexion = "jdbc:sqlite:pruebas.db";
            crearTabla();
            //eliminarTabla();
        } catch (SQLException e) {
            System.err.println("Error de conexión con la base de datos: " + e);
        }

    }

    private void eliminarTabla() 
    {
    	try (Connection conexion = DriverManager.getConnection(cadenaConexion)) {	
    		
    		  PreparedStatement stmt = null;
    		  stmt = conexion.prepareStatement("DROP TABLE cuentas "); 		  
    		  stmt.execute(); 
    		  stmt.close();  
    		  
    		} catch (SQLException sqle) { 
    		  System.out.println("Error en la ejecución: " 
    		    + sqle.getErrorCode() + " " + sqle.getMessage());    
    		}
    }
	private void crearTabla() {
		
        try (Connection conexion = DriverManager.getConnection(cadenaConexion)) {

            String sql = "CREATE TABLE IF NOT EXISTS cuentas (\n"
                    + " id INTEGER PRIMARY KEY AUTOINCREMENT,\n"
                    + " numeroCuenta TEXT NOT NULL UNIQUE,\n"
            		+ " saldo FLOAT NOT NULL,\n"
                    + " tipoCuenta TEXT NOT NULL,\n"
            		+ " idUsuario TEXT NOT NULL,\n"
            		+ " FOREIGN KEY(idUsuario) REFERENCES usuarios(cedula)"
                    + ");";

            Statement sentencia = conexion.createStatement();
            sentencia.execute(sql);

        } catch (SQLException e) {
            System.out.println("Error de conexión: " + e.getMessage());
        }
		
	}

	@Override
	public void guardar(Object objeto)
	{
		
        try (Connection conexion = DriverManager.getConnection(cadenaConexion))
        {
        	UsuarioBaseDatos usuario = new UsuarioBaseDatos();
            Cuenta cuenta = (Cuenta) objeto;
            
            String sentenciaSql = "INSERT INTO cuentas (numeroCuenta, saldo, tipoCuenta, idUsuario) " +
                    " VALUES('" + cuenta.getNumeroCuenta() + "', " + cuenta.getSaldo()
                    + ", '" + cuenta.getTipoCuenta() + "', '" + cuenta.getidUsuario()+"');";
            Statement sentencia = conexion.createStatement();
            if(usuario.buscar(cuenta.getidUsuario())!=null) 
            {
            	sentencia.execute(sentenciaSql);
            }else 
            	{
            		System.out.println("no existe el usuario "+cuenta.getidUsuario()+" para crear una cuenta");
            	}
            
        } catch (SQLException e)
        	{
            	System.err.println("Error de conexión: " + e);
        	}catch (Exception e)
        		{
        			System.err.println("Error " + e.getMessage());
        		}     
	}

	

	@Override
	public void actualizar(Object objeto) {
		
	}

	@Override
	public void depositar(String numeroCuenta, float montoDepositarDinero) 
	{
		
		try (Connection conexion = DriverManager.getConnection(cadenaConexion))
		{
            String sentenciaSQL = "SELECT * FROM cuentas WHERE numeroCuenta = ?";
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
            sentencia.setString(1, numeroCuenta);
            ResultSet resultadoConsulta = sentencia.executeQuery();
            
            if(resultadoConsulta.getString("numeroCuenta").equals(numeroCuenta))
            {
            	String sentenciaSQL2;
            	PreparedStatement sentencia2 = null;
            	int totalDepositos =resultadoConsulta.getInt("numeroDepositos")+1;
            	float totalSaldo = 0;
            	
            	if(resultadoConsulta.getInt("tipoCuenta") == 1) 
            	{
            		try 
            		{
            			sentenciaSQL2 = "UPDATE cuentas set saldo =?,numeroDepositos =? WHERE numeroCuenta = ?";
                		sentencia2 = conexion.prepareStatement(sentenciaSQL2);
            		}catch(SQLException e) 
            			{
            				System.out.println(e.getMessage());
            			}
            		
            		if(resultadoConsulta.getInt("numeroDepositos") < 2) 
            		{         			
            			           				              			
                            totalSaldo = (float) (resultadoConsulta.getFloat("saldo") + (montoDepositarDinero +(montoDepositarDinero*0.005)));                                   
                		  		          		
            		}else 
            			{
            				totalSaldo = (float) (resultadoConsulta.getFloat("saldo") + montoDepositarDinero );                				
            			}
            		
            		try 
            		{
            			sentencia2.setFloat(1, totalSaldo);
        				sentencia2.setInt(2, totalDepositos);
        				sentencia2.setString(3, numeroCuenta);
        				sentencia2.executeUpdate();
            		}catch(SQLException e) 
            			{
            				System.out.println(e.getMessage());
            			}
            	 	
            	}else
            		if(resultadoConsulta.getInt("tipoCuenta") == 2)
            		{
                		try 
                		{
                			sentenciaSQL2 = "UPDATE cuentas set saldo =?,numeroDepositos =? WHERE numeroCuenta = ?";
                    		sentencia2 = conexion.prepareStatement(sentenciaSQL2);
                    		totalDepositos = resultadoConsulta.getInt("numeroDepositos")+1;
                    		totalSaldo = (float) (resultadoConsulta.getFloat("saldo") + montoDepositarDinero);
                    		sentencia2.setFloat(1, totalSaldo);
            				sentencia2.setInt(2, totalDepositos);
            				sentencia2.setString(3, numeroCuenta);
            				sentencia2.executeUpdate();
                    		
                		}catch(SQLException e) 
                			{
                				System.out.println(e.getMessage());
                			}
            		}
            }else 
            	{
            		System.out.println("cuenta no encontrada");
            	}
          
        }catch (SQLException e) {
            System.err.println("Error de conexión: " + e);
        }
	}
	
	
	@Override
	public void retirar(String numeroCuenta, float montoRetirarDinero) 
	{
		try (Connection conexion = DriverManager.getConnection(cadenaConexion))
		{
			String sentenciaSQL = "SELECT * FROM cuentas WHERE numeroCuenta = ?";
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
            sentencia.setString(1, numeroCuenta);
            ResultSet resultadoConsulta = sentencia.executeQuery();
            
            if(resultadoConsulta.getString("numeroCuenta").equals(numeroCuenta))
            {
            	String sentenciaSQL2;
            	PreparedStatement sentencia2 = null;
            	int totalRetiros = 0;
            	//int totalRetiros =resultadoConsulta.getInt("numeroRetiros")+1;
            	float totalSaldo = 0;
            	
            	if(resultadoConsulta.getInt("tipoCuenta") == 1) 
            	{
            		if(resultadoConsulta.getFloat("saldo") < montoRetirarDinero)
            		{
            			System.out.println("fondos insuficientes");
            		}else 
            			{
                			try 
                			{
                				sentenciaSQL2 = "UPDATE cuentas set saldo =?,numeroRetiros =? WHERE numeroCuenta = ?";
                				sentencia2 = conexion.prepareStatement(sentenciaSQL2);
                			}catch(SQLException e) 
                				{
                					System.out.println(e.getMessage());
                				}
                		
                			if(resultadoConsulta.getInt("numeroRetiros") >= 3) 
                			{         			             			           				              			
                                totalSaldo = (float) (resultadoConsulta.getFloat("saldo") - (montoRetirarDinero +(montoRetirarDinero*0.01)));   
                                if(totalSaldo < 0)
                                {
                                	System.out.println("no se puede retirar dinero,fondos insuficientes");
                                }else 
                                	{
                                		try 
                                		{
                                			totalRetiros =resultadoConsulta.getInt("numeroRetiros")+1;
                                			sentencia2.setFloat(1, totalSaldo);
                                			sentencia2.setInt(2, totalRetiros);
                                			sentencia2.setString(3, numeroCuenta);
                                			sentencia2.executeUpdate();
                                		}catch(SQLException e) 
                        					{
                        						System.out.println(e.getMessage());
                        					}
                                	}
                			}else 
                				{
                					totalSaldo = (float) (resultadoConsulta.getFloat("saldo") - montoRetirarDinero );   
                            		try 
                            		{
                            			totalRetiros =resultadoConsulta.getInt("numeroRetiros")+1;
                            			sentencia2.setFloat(1, totalSaldo);
                            			sentencia2.setInt(2, totalRetiros);
                            			sentencia2.setString(3, numeroCuenta);
                            			sentencia2.executeUpdate();
                            		}catch(SQLException e) 
                    					{
                    						System.out.println(e.getMessage());
                    					}
                				}
                		
                			
            			}       		
            	}else
            		if(resultadoConsulta.getInt("tipoCuenta") == 2)
            		{
                		try 
                		{
                			sentenciaSQL2 = "UPDATE cuentas set saldo =?,numeroRetiros =? WHERE numeroCuenta = ?";
                    		sentencia2 = conexion.prepareStatement(sentenciaSQL2);
                    		if(resultadoConsulta.getInt("numeroRetiros") >= 5) 
                    		{
                    			System.out.println("no se pueden hacer mas retiros,limite alcanzado");
                    		}else 
                    			{
                    				if(resultadoConsulta.getFloat("saldo") < montoRetirarDinero) 
                    				{
                    					System.out.println("no se puede retirar el dinero,fondos insuficientes");
                    				}else 
                    					{
                    						totalSaldo = (float) (resultadoConsulta.getFloat("saldo") - montoRetirarDinero);
                                    		totalRetiros = resultadoConsulta.getInt("numeroRetiros")+1;
                    						try 
                                    		{
                                    			sentencia2.setFloat(1, totalSaldo);
                                    			sentencia2.setInt(2, totalRetiros);
                                    			sentencia2.setString(3, numeroCuenta);
                                    			sentencia2.executeUpdate();
                                    		}catch(SQLException e) 
                            					{
                            						System.out.println(e.getMessage());
                            					}
                    						
                    					}
                    			}
                    		
                		}catch(SQLException e) 
                			{
                				System.out.println(e.getMessage());
                			}
            		}
            }else 
            	{
            		System.out.println("cuenta no encontrada");
            	}
          
        }catch (SQLException e) {
            System.err.println("Error de conexión: " + e);
        }
	}
	
	@Override
	public void transferir(String numeroCuentaUno,String numeroCuentaDos,float montoTransferir) 
	{
		try(Connection conexion = DriverManager.getConnection(cadenaConexion)) 
		{
			String sentenciaSQL = "SELECT * FROM cuentas WHERE numeroCuenta = ?";
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
            sentencia.setString(1, numeroCuentaUno);
            ResultSet resultadoConsulta = sentencia.executeQuery();
            
            String sentenciaSQL2 = "SELECT * FROM cuentas WHERE numeroCuenta = ?";
            PreparedStatement sentencia2 = conexion.prepareStatement(sentenciaSQL2);
            sentencia2.setString(1, numeroCuentaDos);
            ResultSet resultadoConsulta2 = sentencia2.executeQuery();
            
            String sentenciaSQL3 = "UPDATE cuentas set saldo =?,numeroTransferenciasCorrienteAhorro=? WHERE numeroCuenta = ?";
            PreparedStatement sentencia3 = conexion.prepareStatement(sentenciaSQL3);
            
            String sentenciaSQL4 = "UPDATE cuentas set saldo =? WHERE numeroCuenta = ?";
            PreparedStatement sentencia4 = conexion.prepareStatement(sentenciaSQL4);

            Float transferencia = (float) 0,actualizarSaldo = (float) 0;
            
            if(numeroCuentaUno.equals(numeroCuentaDos)) 
            {
            	System.out.println("no puede realizar una transferencia a usted mismo");
            }else
            	if(resultadoConsulta.getInt("tipoCuenta") == 1)
            	{
            		if(resultadoConsulta2.getInt("tipoCuenta") == 2)
            		{
            			if(resultadoConsulta.getFloat("saldo") < montoTransferir)
            			{
            				System.out.println("no se puede transferir,fondos insuficientes");
            			}else 
            				{
            					actualizarSaldo = (float) (resultadoConsulta.getFloat("saldo") - (montoTransferir+(montoTransferir*0.015)));
            					transferencia = resultadoConsulta2.getFloat("saldo") + montoTransferir;
            					try 
            					{
                 					sentencia3.setFloat(1, actualizarSaldo);
                        			sentencia3.setString(3, numeroCuentaUno);
                        			sentencia3.executeUpdate();
                        			
                        			sentencia4.setFloat(1, transferencia);
                        			sentencia4.setString(2, numeroCuentaDos);
                        			sentencia4.executeUpdate();
            					}catch(SQLException e) 
            						{
            							System.out.println("errror al transferir"+e.getMessage());
            						}
                    			
            				}
            		}else
            			if(resultadoConsulta2.getInt("tipoCuenta") == 1) 
            			{
            				if(resultadoConsulta.getFloat("saldo") < montoTransferir)
                			{
                				System.out.println("no se puede transferir,fondos insuficientes");
                			}else 
                				{
                				actualizarSaldo = (float) (resultadoConsulta.getFloat("saldo") - montoTransferir);
            					transferencia = resultadoConsulta2.getFloat("saldo") + montoTransferir;
            					
            					try 
            					{
                 					sentencia3.setFloat(1, actualizarSaldo);
                        			sentencia3.setString(3, numeroCuentaUno);
                        			sentencia3.executeUpdate();
                        			
                        			sentencia4.setFloat(1, transferencia);
                        			sentencia4.setString(2, numeroCuentaDos);
                        			sentencia4.executeUpdate();
            					}catch(SQLException e) 
            						{
            							System.out.println("errror al transferir"+e.getMessage());
            						}
                				}
            			}
            	}else
            		if(resultadoConsulta.getInt("tipoCuenta") == 2) 
            		{
            			if(resultadoConsulta2.getInt("tipoCuenta") == 1) 
            			{
            				if(resultadoConsulta.getFloat("saldo") < montoTransferir)
                			{
                				System.out.println("no se puede transferir,fondos insuficientes");
                			}else 
                				{
                					actualizarSaldo = (float) (resultadoConsulta.getFloat("saldo") - (montoTransferir+(montoTransferir*0.02)));
                					if(actualizarSaldo < 0) 
                					{
                						System.out.println("no se puede transferir,fondos insuficientes");
                					}else 
                						{
                							if(resultadoConsulta.getInt("numeroTransferenciasCorrienteAhorro") >= 2) 
                							{
                								System.out.println("no se puede hacer la transferencia");
                							}else 
                    							{
                    								int numeroTransferenciasCorrienteAhorro = resultadoConsulta.getInt("numeroTransferenciasCorrienteAhorro")+1;
                    								actualizarSaldo = (float) (resultadoConsulta.getFloat("saldo") - (montoTransferir+(montoTransferir*0.02)));
                    								transferencia = resultadoConsulta2.getFloat("saldo") + montoTransferir;
                    								try 
                    								{
                    									sentencia3.setFloat(1, actualizarSaldo);
                    									sentencia3.setInt(2,numeroTransferenciasCorrienteAhorro );
                    									sentencia3.setString(3, numeroCuentaUno);
                    									sentencia3.executeUpdate();
                        						
                    									sentencia4.setFloat(1, transferencia);
                    									sentencia4.setString(2, numeroCuentaDos);
                    									sentencia4.executeUpdate();
                    								}catch(SQLException e) 
                    									{
                    										System.out.println("errror al transferir"+e.getMessage());
                    									}
                    							}
                						}
                				}
            			}else 
            				if(resultadoConsulta2.getInt("tipoCuenta") == 2)
            				{
            					if(resultadoConsulta.getFloat("saldo") < montoTransferir) 
            					{
            						System.out.println("no se puede transferir,fondos insuficientes");
            					}else 
            						{
        								actualizarSaldo = (float) (resultadoConsulta.getFloat("saldo") - (montoTransferir+(montoTransferir*0.02)));
        								if(actualizarSaldo < 0 ) 
        								{
        									System.out.println("no se puede hacer la transferencia,fondos insufucientes");
        								}else 
        									{
        										transferencia = resultadoConsulta2.getFloat("saldo") + montoTransferir;
        										try 
        										{
        											sentencia3.setFloat(1, actualizarSaldo);
        											sentencia3.setString(3, numeroCuentaUno);
        											sentencia3.executeUpdate();
                						
        											sentencia4.setFloat(1, transferencia);
        											sentencia4.setString(2, numeroCuentaDos);
        											sentencia4.executeUpdate();
        										}catch(SQLException e) 
            										{
            											System.out.println("errror al transferir"+e.getMessage());
            									}
        									}
        								
            						}
            				}
            			
            		}
		}catch(SQLException e)
			{
				System.out.println("error de conexion "+e.getMessage());
			};
	}
	
	@Override
	public Object buscar(String numeroCuenta) {
		
		try (Connection conexion = DriverManager.getConnection(cadenaConexion)) {
            String sentenciaSQL = "SELECT * FROM cuentas WHERE numeroCuenta = ?";
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
            sentencia.setString(1, numeroCuenta);
            ResultSet resultadoConsulta = sentencia.executeQuery();
            if (resultadoConsulta != null && resultadoConsulta.next()) {
                Cuenta cuenta = null;
                
                String numeroCuentaResultado = resultadoConsulta.getString("numeroCuenta");
            	float saldo = resultadoConsulta.getFloat("saldo") ;
            	String tipoCuenta = resultadoConsulta.getString("tipoCuenta");
            	String idUsuario = resultadoConsulta.getString("idUsuario");
            	cuenta = new Cuenta(numeroCuentaResultado, saldo, tipoCuenta, idUsuario);
                return cuenta;
            }

        } catch (SQLException e) {
            System.err.println("Error de conexión: " + e);
        }
		
		return null;
	}
	
	 @Override
	    public void eliminar(String numeroCuenta) {
	        try (Connection conexion = DriverManager.getConnection(cadenaConexion)) {
	            String sentenciaSql = "DELETE FROM cuentas WHERE numeroCuenta = '" + numeroCuenta + "';";
	            Statement sentencia = conexion.createStatement();
	            sentencia.execute(sentenciaSql);
	        } catch (SQLException e) {
	            System.err.println("Error de conexión: " + e);
	        } catch (Exception e) {
	            System.err.println("Error " + e.getMessage());
	        }
	    }
	
	@Override
	public List<?> listar(String idUsuario) {
		
        List<Cuenta> cuentas = new ArrayList<Cuenta>();
        String respuesta;
        try (Connection conexion = DriverManager.getConnection(cadenaConexion)) {
            String sentenciaSql = "SELECT * FROM Cuentas";
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            ResultSet resultadoConsulta = sentencia.executeQuery();

            if (resultadoConsulta != null) {
                while (resultadoConsulta.next()) 
                {
                	respuesta = resultadoConsulta.getString("idUsuario");
                	if(idUsuario.equals(respuesta))
                	{
                        Cuenta cuenta = null;  
                        String numeroCuenta = resultadoConsulta.getString("numeroCuenta");
                    	float saldo = resultadoConsulta.getFloat("saldo") ;
                    	String tipoCuenta = resultadoConsulta.getString("tipoCuenta");
                    	String idUsuarioResultado = resultadoConsulta.getString("idUsuario");
                    	cuenta = new Cuenta(numeroCuenta, saldo, tipoCuenta, idUsuarioResultado);
                        cuentas.add(cuenta);               			
                	}

                }
                return cuentas;
            }
        } catch (SQLException e) {
            System.err.println("Error de conexión: " + e);
        }
        return null;
	}

}
