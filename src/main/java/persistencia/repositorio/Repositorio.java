package persistencia.repositorio;

import java.util.List;

//repositorio de la clase cuenta
public interface Repositorio {
	
	public void guardar(Object objeto);

    public void eliminar(String identificador);

    public void actualizar(Object objeto);

    public Object buscar(String identificador);
    
    public void retirar(String numeroCuenta,float montoRetirarDinero);

    public void depositar(String numeroCuenta,float montoRetirarDinero);
    
    public void transferir(String numeroCuentaUno,String numeroCuentaDos,float montoTransferir);
    public List<?> listar(String idUsuario);

}
