package persistencia.repositorio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import persistencia.entidades.Usuario;

public class UsuarioBaseDatos implements RepositorioUsuario {

	private String cadenaConexion;
	
	public UsuarioBaseDatos() {
        try {
            DriverManager.registerDriver(new org.sqlite.JDBC());
            cadenaConexion = "jdbc:sqlite:pruebas.db";
            crearTabla();
            //eliminarTabla();
        } catch (SQLException e) {
            System.err.println("Error de conexión con la base de datos: " + e);
        }

    }
	
    private void eliminarTabla() 
    {
    	try (Connection conexion = DriverManager.getConnection(cadenaConexion)) {	
    		
    		  PreparedStatement stmt = null;
    		  stmt = conexion.prepareStatement("DROP TABLE usuarios "); 		  
    		  stmt.execute(); 
    		  stmt.close();  
    		  
    		} catch (SQLException sqle) { 
    		  System.out.println("Error en la ejecución: " 
    		    + sqle.getErrorCode() + " " + sqle.getMessage());    
    		}
    }
	private void crearTabla() {
		
        try (Connection conexion = DriverManager.getConnection(cadenaConexion)) {

            String sql = "CREATE TABLE IF NOT EXISTS usuarios (\n"
                    + " id INTEGER PRIMARY KEY AUTOINCREMENT,\n"
                    + " nombre TEXT NOT NULL,\n"
            		+ " apellido TEXT NOT NULL,\n"
                    + " cedula TEXT NOT NULL UNIQUE"
                    + ");";

            Statement sentencia = conexion.createStatement();
            sentencia.execute(sql);

        } catch (SQLException e) {
            System.out.println("Error de conexión: " + e.getMessage());
        }
		
	}
	@Override
	public void guardar(Object objeto) 
	{
		try (Connection conexion = DriverManager.getConnection(cadenaConexion))
        {
            Usuario usuario = (Usuario) objeto;
            String sentenciaSql = "INSERT INTO usuarios (nombre, apellido, cedula ) " +
                    " VALUES('" + usuario.getNombre()+ "', '" + usuario.getApellido()
                    + "', '" + usuario.getCedula() + "');";
            Statement sentencia = conexion.createStatement();
            sentencia.execute(sentenciaSql);
        } catch (SQLException e)
        	{
            	System.err.println("Error de conexión: " + e);
        	}catch (Exception e)
        		{
        			System.err.println("Error " + e.getMessage());
        		}
		
	}

	@Override
	public void eliminar(String cedula) {
		
		try (Connection conexion = DriverManager.getConnection(cadenaConexion)) {
            String sentenciaSql = "DELETE FROM usuarios WHERE cedula = '" + cedula + "';";
            Statement sentencia = conexion.createStatement();
            sentencia.execute(sentenciaSql);
        } catch (SQLException e) {
            System.err.println("Error de conexión: " + e);
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }
		
	}

	@Override
	public void actualizar(Object objeto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object buscar(String cedula) {
		
		try (Connection conexion = DriverManager.getConnection(cadenaConexion)) {
            String sentenciaSQL = "SELECT * FROM usuarios WHERE cedula = ?";
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
            sentencia.setString(1, cedula);
            ResultSet resultadoConsulta = sentencia.executeQuery();
            if (resultadoConsulta != null && resultadoConsulta.next()) {
                Usuario usuario = null;
                
                String nombre = resultadoConsulta.getString("nombre");
                String apellido = resultadoConsulta.getString("apellido");
                String cedulaResultado = resultadoConsulta.getString("cedula");
                
                usuario = new Usuario(nombre,apellido,cedulaResultado);
                return usuario;
            }

        } catch (SQLException e) {
            System.err.println("Error de conexión: " + e);
        }
		
		return null;
	}

	@Override
	public List<?> listar() {
		
		List<Usuario> usuarios = new ArrayList<Usuario>();

        try (Connection conexion = DriverManager.getConnection(cadenaConexion)) {
            String sentenciaSql = "SELECT * FROM usuarios";
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            ResultSet resultadoConsulta = sentencia.executeQuery();

            if (resultadoConsulta != null) {
                while (resultadoConsulta.next()) {
                    Usuario usuario = null;                                        
                    String nombre = resultadoConsulta.getString("nombre");
                    String apellido = resultadoConsulta.getString("apellido");
                    String cedulaResultado = resultadoConsulta.getString("cedula");                  
                    usuario = new Usuario(nombre,apellido,cedulaResultado);
                    usuarios.add(usuario);
                }
                return usuarios;
            }
        } catch (SQLException e) {
            System.err.println("Error de conexión: " + e);
        }
        return null;
	}

}
