package persistencia.repositorio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import persistencia.entidades.Cuenta;
import persistencia.entidades.transacciones;

public class transaccionesBaseDatos implements repositorioTransacciones  {

	private String cadenaConexion;

    public transaccionesBaseDatos() {
        try {
            DriverManager.registerDriver(new org.sqlite.JDBC());
            cadenaConexion = "jdbc:sqlite:pruebas.db";
            crearTabla();
            //eliminarTabla();
        } catch (SQLException e) {
            System.err.println("Error de conexión con la base de datos: " + e);
        }

    }
    
    private void eliminarTabla() 

    {
    	try (Connection conexion = DriverManager.getConnection(cadenaConexion)) {	
    		
    		  PreparedStatement stmt = null;
    		  stmt = conexion.prepareStatement("DROP TABLE transacciones "); 		  
    		  stmt.execute(); 
    		  stmt.close();  
    		  
    		} catch (SQLException sqle) { 
    		  System.out.println("Error en la ejecución: " 
    		    + sqle.getErrorCode() + " " + sqle.getMessage());    
    		}
    }
    
	private void crearTabla() {
		
        try (Connection conexion = DriverManager.getConnection(cadenaConexion)) {

            String sql = "CREATE TABLE IF NOT EXISTS transacciones (\n"
                    + " id INTEGER PRIMARY KEY AUTOINCREMENT,\n"
                    + " fecha TEXT NOT NULL,\n"
            		+ " hora  TEXT NOT NULL,\n"
                    + " tipoTransaccion TEXT NOT NULL,\n"
            		+ " monto FLOAT NOT NULL ,\n"
                    + " numeroCuenta TEXT NOT NULL,\n"
            		+ " tipoCuentaDestino TEXT,\n"
            		+ " FOREIGN KEY(numeroCuenta) REFERENCES cuentas(numeroCuenta)"
                    + ");";

            Statement sentencia = conexion.createStatement();
            sentencia.execute(sql);

        } catch (SQLException e) {
            System.out.println("Error de conexión: " + e.getMessage());
        }
		
	}

	@Override
	public void retirar(String numeroCuenta, float montoRetirarDinero) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void guardarDeposito(Object objeto) {
		
		try (Connection conexion = DriverManager.getConnection(cadenaConexion))
		{
			float totalSaldo;
			CuentaBaseDatos cuenta = new CuentaBaseDatos();
            transacciones transaccion = (transacciones) objeto;
            
            String numeroCuenta = transaccion.getIdCuenta();
            
            String sentenciaSQL = "SELECT * FROM cuentas WHERE numeroCuenta = ?";
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
            sentencia.setString(1, transaccion.getIdCuenta());
            ResultSet resultadoConsulta = sentencia.executeQuery();
            
            if(resultadoConsulta.getString("numeroCuenta").equals(numeroCuenta) ) 
            {
            	String sentenciaSQL2 = "UPDATE cuentas set saldo =? WHERE numeroCuenta = ?";
            	PreparedStatement sentencia2 = null;
        		
        		totalSaldo = (float) (resultadoConsulta.getFloat("saldo") + transaccion.getMonto());
        		sentencia2.setFloat(1, totalSaldo);
				sentencia2.setString(3, transaccion.getIdCuenta());
				sentencia2.executeUpdate();
				
				String sentenciaSQL3 = "INSERT INTO transacciones (fecha, hora, tipoTransaccion, monto, idCuenta, tipoCuentaDestino) " +
	                    " VALUES('" + transaccion.getFecha() + "', '" + transaccion.getHora()
	                    + "', '" + transaccion.getTipoTransaccion()+ "', " + transaccion.getMonto()
	                    +",'"+transaccion.getIdCuenta()+"','"+transaccion.getTipoCuentaDestino()+"');";
	            Statement sentencia3 = conexion.createStatement();
	            sentencia3.execute(sentenciaSQL3);
            }           

        }catch (SQLException e) {
            System.err.println("Error de conexión: " + e);
        }
		
	}

	@Override
	public void transferir(String numeroCuentaUno, String numeroCuentaDos, float montoTransferir) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<?> listar() {
		// TODO Auto-generated method stub
		return null;
	}


	
	
	
}
