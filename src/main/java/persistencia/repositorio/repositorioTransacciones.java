package persistencia.repositorio;

import java.util.List;

public interface repositorioTransacciones {

	//public void guardar(Object objeto);

    //public void eliminar(String identificador);

    //public Object buscar(String identificador);
	
    
    public void retirar(String numeroCuenta,float montoRetirarDinero);

    public void guardarDeposito(Object objeto);
    
    public void transferir(String numeroCuentaUno,String numeroCuentaDos,float montoTransferir);
    
    public List<?> listar();
}
