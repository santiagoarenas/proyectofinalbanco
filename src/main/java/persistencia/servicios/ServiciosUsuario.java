package persistencia.servicios;

import java.util.List;
import java.util.Map;

import persistencia.entidades.Usuario;
import persistencia.repositorio.RepositorioUsuario;
import persistencia.repositorio.UsuarioBaseDatos;

public class ServiciosUsuario {

	private RepositorioUsuario repositorioUsuario;

	public ServiciosUsuario()
	{
		repositorioUsuario = new UsuarioBaseDatos();
	}
	//metodo para guardar usuario desde eclipse
    public void guardarUsuario(Usuario nuevoUsuario) {
        repositorioUsuario.guardar(nuevoUsuario);
    }
    //metodo para guardar usuario desde imsomnio
    public void guardarUsuario(Map datos) {
    	
        String nombre = (String) datos.get("nombre");
        String apellido = (String) datos.get("apellido");
        String cedula = (String) datos.get("cedula");
        Usuario nuevoUsuario = new Usuario(nombre, apellido, cedula);
        repositorioUsuario.guardar(nuevoUsuario);
    }
    public List<Usuario> listarUsuarios() {
        return (List<Usuario>) repositorioUsuario.listar();
    }

    public Usuario buscarUsuario(String cedula) throws Exception {
        Object usuario = repositorioUsuario.buscar(cedula);
        if(usuario == null) {
            throw new Exception("No se encontro la persona");
        }
        return (Usuario) usuario;
    }
    
    public void eliminarUsuario(String cedula) {
    	repositorioUsuario.eliminar(cedula);
    }
}
