package persistencia.servicios;

import java.util.List;
import java.util.Map;

import persistencia.entidades.Cuenta;
import persistencia.repositorio.CuentaBaseDatos;
import persistencia.repositorio.Repositorio;

public class ServiciosCuentas {

	private Repositorio repositorioCuenta;

    public ServiciosCuentas() {
    	repositorioCuenta = new CuentaBaseDatos();
    }

    //metodo para guardar persona desde eclipse
    public void guardarCuenta(Cuenta newCuenta) {
        repositorioCuenta.guardar(newCuenta);
    }
    //metodo para guardar persona desde imsomnio
    public void guardarCuenta(Map datos) {
    	
        String numeroCuenta = (String) datos.get("numeroCuenta");
        String tipoCuenta = (String) datos.get("tipoCuenta");
        String idUsuario = (String) datos.get("idUsuario");
        Cuenta nuevaCuenta = new Cuenta(numeroCuenta,tipoCuenta,idUsuario);
        repositorioCuenta.guardar(nuevaCuenta);
    }
    public List<Cuenta> listarCuentas(String idUsuario) {
        return (List<Cuenta>) repositorioCuenta.listar(idUsuario);
    }

    public Cuenta buscarCuenta(String numeroCuenta) throws Exception {
        Object cuenta = repositorioCuenta.buscar(numeroCuenta);
        if(cuenta == null) {
            throw new Exception("No se encontro la persona");
        }
        return (Cuenta) cuenta;
    }
    
    public void retirarDinero(String numeroCuenta,float montoRetirarDinero) 
    {
    	repositorioCuenta.retirar(numeroCuenta, montoRetirarDinero);
    }
    
    public void depositarDinero(String numeroCuenta,float montoRetirarDinero) 
    {
    	repositorioCuenta.depositar(numeroCuenta, montoRetirarDinero);
    }

    public void transferirDinero(String numeroCuentaUno,String numeroCuentaDos,float montoTransferir) 
    {
    	repositorioCuenta.transferir(numeroCuentaUno, numeroCuentaDos, montoTransferir);
    }
    public void eliminarcuenta(String numeroCuenta) {
    	repositorioCuenta.eliminar(numeroCuenta);
    }
}
