package persistencia.entidades;

public class Cuenta {

	private String numeroCuenta;
	private float saldo;
	private String tipoCuenta;
	private String idUsuario;
	
	//constructor para crear una cuenta
	public Cuenta(String numeroCuenta, String tipoCuenta, String idUsuario) {

		this.numeroCuenta = numeroCuenta;
		this.saldo = 0;
		this.tipoCuenta = tipoCuenta;
		this.idUsuario = idUsuario;
	}
	//constructor para usar metodos diferentes de crear cuenta
	public Cuenta(String numeroCuenta,float saldo, String tipoCuenta,String idUsuario) {

		this.numeroCuenta = numeroCuenta;
		this.saldo = saldo;
		this.tipoCuenta = tipoCuenta;
		this.idUsuario = idUsuario;
	}
	
	

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public float getSaldo() {
		return saldo;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public String getidUsuario() {
		return idUsuario;
	}

	public void setidUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	
	
	

	
}
