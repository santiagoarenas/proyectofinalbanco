package persistencia.entidades;

import java.time.LocalDate;
import java.time.LocalTime;

public class transacciones {

	private String fecha;
	private String hora;
	private String tipoTransaccion;
	private float monto;
	private String idCuenta;
	private String tipoCuentaDestino;
	
	public transacciones(String fecha, String hora, String tipoTransaccion, float monto, String idCuenta,
			String tipoCuentaDestino) {
		
		this.fecha = fecha;
		this.hora = hora;
		this.tipoTransaccion = tipoTransaccion;
		this.monto = monto;
		this.idCuenta = idCuenta;
		this.tipoCuentaDestino = tipoCuentaDestino;
	}


	public transacciones() {}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getTipoTransaccion() {
		return tipoTransaccion;
	}

	public void setTipo_transaccion(String tipo_transaccion) {
		this.tipoTransaccion = tipo_transaccion;
	}

	public float getMonto() {
		return monto;
	}

	public void setMonto(float monto) {
		this.monto = monto;
	}

	public String getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(String idCuenta) {
		this.idCuenta = idCuenta;
	}

	public String getTipoCuentaDestino() {
		return tipoCuentaDestino;
	}

	public void setTipoCuentaDestino(String tipoCuentaDestino) {
		this.tipoCuentaDestino = tipoCuentaDestino;
	}
	
	
	
}
	
	
	
	